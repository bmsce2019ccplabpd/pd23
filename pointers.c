#include<stdio.h>
void swap_callby_ref(int*,int*);
int main()
{
int a=1,b=2;
printf("in main,a=%d and b=%d",a,b);
swap_callby_ref(&a,&b);
return 0;
}
swap_callby_ref(int*a,int*b)
{
int temp;
temp=*a;
*a=*b;
*b=temp;
printf("in fun(call_by_ref)a=%d and b=%d",*a,*b);
}